package com.thedailynerd.ivy.github;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import me.drakeet.retrofit2.adapter.agera.AgeraCallAdapterFactory;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by Nick on 7/2/16.
 */
public class Networker {

    public static String GITHUB_BASE_URL = "https://api.github.com/";
    private ObjectMapper mapper;
    private Executor networkExecutor = Executors.newSingleThreadExecutor();

    public Networker(){
        mapper = new ObjectMapper();
        //We don't care about the whole response, ignore anything we don't know how to map
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public GithubService getGitHub(){
        Retrofit retro = new Retrofit.Builder()
                .baseUrl(GITHUB_BASE_URL)
                .addCallAdapterFactory(AgeraCallAdapterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create(mapper))
                .build();
        return retro.create(GithubService.class);
    }

    public Executor executor() {
        return networkExecutor;
    }
}
