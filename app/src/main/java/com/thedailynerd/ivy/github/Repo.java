package com.thedailynerd.ivy.github;

import android.view.View;

import com.thedailynerd.ivybinding.builder.OnBindListener;

/**
 * Created by Nick on 7/2/16.
 */
public class Repo implements OnBindListener, View.OnClickListener {

    public Repo(){ /*Empty Jackson constructor */ }

    public Repo(String name, String description){
        this.name = name;
        this.description = description;
    }

    public String name;
    public String description;

    @Override
    public void onBind(View inflatedView, Object TODO) {
        //Nothing really here atm
    }

    @Override
    public void onClick(View v) {
        //We need to open up a new Repository to get the branches.
    }
}
