package com.thedailynerd.ivy.github;

import java.util.List;

/**
 * Created by Nick on 7/2/16.
 */
public class Branch {
    public String name;
    public List<Commits> commit;
}
