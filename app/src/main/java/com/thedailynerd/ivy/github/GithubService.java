package com.thedailynerd.ivy.github;

import com.google.android.agera.Result;
import com.google.android.agera.Supplier;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Nick on 7/2/16.
 */
public interface GithubService {

    @GET("/users/{user}/repos")
    Supplier<Result<List<Repo>>> getRepos(@Path("user") String user);

    @GET("/repos/{owner}/{repo}/branches")
    Supplier<Result<List<BranchName>>> getBranches(@Path("owner") String user, @Path("repo") String repo);

    @GET("/repos/{owner}/{repo}/branches/{branch}")
    Supplier<Result<List<Branch>>> getBranches(@Path("owner") String user, @Path("repo") String repo, @Path("branch") String branch);
}
