package com.thedailynerd.ivy;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.agera.Function;
import com.google.android.agera.Repositories;
import com.google.android.agera.Repository;
import com.thedailynerd.ivy.github.Branch;
import com.thedailynerd.ivy.github.Networker;
import com.thedailynerd.ivy.github.Repo;
import com.thedailynerd.ivybinding.layout.IvyAutoLayout;
import com.thedailynerd.ivybinding.IvyBinder;
import com.thedailynerd.ivybinding.builder.OnBindListener;

import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "IvyApp";
    private static final List<Repo> AGERA_INIT = new ArrayList<Repo>();
    private Networker mNetworker;
    private Repository repository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mNetworker = new Networker();
        repository = Repositories.repositoryWithInitialValue(AGERA_INIT)
                .observe()
                .onUpdatesPerLoop()
                .goTo(mNetworker.executor())
                .attemptGetFrom(mNetworker.getGitHub().getRepos("ntkachov"))
                .orSkip()
                .thenTransform(new Function<List<Repo>, List<Repo>>(){
                    @NonNull
                    @Override
                    public List<Repo> apply(@NonNull List<Repo> input) {
                        return input;
                    }
                })
                .compile();

//        IvyAutoLayout autoLayout = (IvyAutoLayout) findViewById(R.id.autoLayout);
        ViewGroup testGroup = (ViewGroup) findViewById(R.id.testGroupLayout);
        RecyclerView testRecyclerView = (RecyclerView) findViewById(R.id.testRecyclerLayout);

        List<Repo> testList = new ArrayList<>();
        testList.add(new Repo("RX Test Repo", "This is a trial"));
        testList.add(new Repo("RX Test Repo", "This is a trial"));
        testList.add(new Repo("RX Test Repo", "This is a trial"));

        IvyBinder.initializeWith(Observable.just(testList))
                .render(Repo.class).bindTo(R.layout.repo, com.thedailynerd.ivy.BR.repo).onBind(new OnBindListener() {
            @Override
            public void onBind(View inflatedView, Object dataItem) {
                inflatedView.setBackgroundColor(Color.MAGENTA);
            }
        }).onClick(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.setBackgroundColor(Color.CYAN);
            }
        }).inflateInto(testRecyclerView)
//                .inside(R.layout.repo).render(Branch.class).bindTo(R.layout.branch, com.thedailynerd.ivy.BR.branch).inflateInto(R.id.branches)
                .generateBindings(this);

    }

}
